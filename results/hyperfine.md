| Command | Mean [s] | Min [s] | Max [s] | Relative |
|:---|---:|---:|---:|---:|
| `bazel build  --spawn_strategy=local //...` | 4.813 ± 0.140 | 4.529 | 5.007 | 1.02 ± 0.03 |
| `bazel build --extra_toolchains=@llvm_toolchain//:cc-toolchain-x86_64-linux --spawn_strategy=local //...` | 6.921 ± 0.238 | 6.513 | 7.279 | 1.46 ± 0.05 |
| `bazel build --extra_toolchains=@llvm_toolchain_with_sysroot//:cc-toolchain-x86_64-linux --spawn_strategy=local //...` | 9.336 ± 0.347 | 8.968 | 10.220 | 1.98 ± 0.08 |
| `bazel build --platforms=@zig_sdk//libc_aware/platform:linux_amd64_gnu.2.28 --spawn_strategy=local //...` | 9.311 ± 0.190 | 9.050 | 9.667 | 1.97 ± 0.05 |
| `bazel build  --experimental_reuse_sandbox_directories //...` | 4.726 ± 0.051 | 4.653 | 4.793 | 1.00 |
| `bazel build --extra_toolchains=@llvm_toolchain//:cc-toolchain-x86_64-linux --experimental_reuse_sandbox_directories //...` | 7.674 ± 0.370 | 7.000 | 8.492 | 1.62 ± 0.08 |
| `bazel build --extra_toolchains=@llvm_toolchain_with_sysroot//:cc-toolchain-x86_64-linux --experimental_reuse_sandbox_directories //...` | 13.098 ± 0.329 | 12.365 | 13.510 | 2.77 ± 0.08 |
| `bazel build --platforms=@zig_sdk//libc_aware/platform:linux_amd64_gnu.2.28 --experimental_reuse_sandbox_directories //...` | 13.465 ± 0.292 | 13.033 | 13.897 | 2.85 ± 0.07 |
| `bazel build  --spawn_strategy=sandboxed //...` | 4.766 ± 0.179 | 4.553 | 5.224 | 1.01 ± 0.04 |
| `bazel build --extra_toolchains=@llvm_toolchain//:cc-toolchain-x86_64-linux --spawn_strategy=sandboxed //...` | 8.666 ± 0.116 | 8.504 | 8.883 | 1.83 ± 0.03 |
| `bazel build --extra_toolchains=@llvm_toolchain_with_sysroot//:cc-toolchain-x86_64-linux --spawn_strategy=sandboxed //...` | 33.827 ± 0.630 | 32.121 | 34.412 | 7.16 ± 0.15 |
| `bazel build --platforms=@zig_sdk//libc_aware/platform:linux_amd64_gnu.2.28 --spawn_strategy=sandboxed //...` | 20.396 ± 0.330 | 19.966 | 20.939 | 4.32 ± 0.08 |
