#!/bin/bash
set -x

exec hyperfine \
    --export-markdown=results/hyperfine.md \
    --runs 10 \
    -L toolchain ,--extra_toolchains=@llvm_toolchain//:cc-toolchain-x86_64-linux,--extra_toolchains=@llvm_toolchain_with_sysroot//:cc-toolchain-x86_64-linux,--platforms=@zig_sdk//libc_aware/platform:linux_amd64_gnu.2.28 \
    -L sandbox_strategy "--spawn_strategy=local,--experimental_reuse_sandbox_directories,--spawn_strategy=sandboxed" \
    -p "bazel build {toolchain} {sandbox_strategy} //:zigzag-0; bazel clean; bazel shutdown" \
       "bazel build {toolchain} {sandbox_strategy} //..."
